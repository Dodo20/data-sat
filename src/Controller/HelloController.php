<?php

namespace App\Controller;

use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class HelloController extends AbstractController
{
    /**
     * titre qui sera affich� dans l'en-ete du navigateur
     * 
     * @var string
     */
    private $title = "Accueil"; //définit une variable
    
    private $today;
    
    private $entityManager;
    
    public function __construct(){ //constructeur
        $this->today = new \DateTime();
    }
        
    /**
     * @Route("/hello", name="hello")
     */
    public function index() //methode
    {
        return new Response("Hello Symfony nice to meet");
    }
    
    /**
     * @Route("/hello/{name}", name="hello-name")
     * 
     * @return Response
     */    
    public function helloWho($name): Response{
        
        return $this->render(
            "home/myhome.html.twig",
            [
                "title"=>$this->title,
                "name"=>$name
            ]
        );
    }
    
    /**
     * @Route("/", name="home")
     */
    public function home(){
        $this->entityManager = $this->getDoctrine()->getManager();
        
        return $this->render(
            "home/home.html.twig",
            [
                "title"=>$this->title,
                "categories"=>$this->getCategories(),
                "begin"=>new \DateTime()
            ]
        );
        
    }
    
    private function getDatas(): array{
        $today = new \DateTime();
       
        
        $datas = [clone $today]; //tableau continet la date du jour
        
        for ($i=0; $i<10; $i++){
            $today->modify("-1 day");
            $datas[] = clone $today; //on range dans tableau date - 1 jour
        }
        return $datas;
    }
    
    //private function getCategories() {
        //return $this //fait référence au controleur
           // ->getDoctrine() // service qui récupère l'instance de Doctrine qui donne accès au gestionaire
            //->getManager() // gestionnaire du repository
            //->getRepository(Category::class)
           // ->findAll();
    //}
    
    
    private function getCategories() {   
        //Ajouter une donnée dans une DB
        /*$newCategory = new Category();
        $newCategory->setLibelle("Categorie 3"); //setter
        
        $this->entityManager
            ->persist($newCategory);
        $this->entityManager->flush(); //commit le persist*/
        
        return $this->entityManager
        ->getRepository(Category::class)
        ->findAll();
    }
}
