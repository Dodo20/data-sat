<?php
namespace App\Repository;

use App\Entity\Results;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Results|null find($id, $lockMode = null, $lockVersion = null)
 * @method Results|null findOneBy(array $criteria, array $orderBy = null)
 * @method Results[]    findAll()
 * @method Results[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResultsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Results::class);
    }

    // /**
    //  * @return Results[] Returns an array of Results objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    public function findBetween(\DateTime $referenceDate=null){
        $today = is_null($referenceDate) ? new \DateTime() : $referenceDate;
        
        $strDate = $today -> format('Y') . "-" . $today -> format('m') . "-01";
        
        $beginDate = new \DateTime(
            $strDate
        );
        
        $endDate = clone $beginDate;
        $endDate->modify("+ 1 month")->modify("-1 day");
        
        return $this->createQueryBuilder('rs')
            ->where('rs.date BETWEEN :beginDate AND :endDate')
            ->setParameter('beginDate', $beginDate->format('Y-m-d'))
            ->setParameter('endDate', $endDate->format('Y-m-d'))
            ->orderBy('rs.date', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?Results
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
