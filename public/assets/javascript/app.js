/**
 * @name app.js
 * @abstract Point d'entrée Javascript
 */

$(document).ready(function(){
console.info('JS app is running');

	//tableau des results à retourner
	let results = [];


//handling search-form
	$('#search-form').on(
		'submit', //evènement à écouter
		(event) => {//gestionnaire de l'évènement
			$('#data-loader').removeClass('silent');
			
			event.preventDefault(); //interdire l'évènement par défaut càd l'envoie du formulaire
			
			//call symfony API with parameters
			console.log('My own manager');
			
			$.ajax({
				url: 'http://datasat.wrk/results/' + $('#begin').val() + "/" + $('#end').val,
				method: 'get',
				dataType: 'json',
				success: (datas) =>{
					//Clear the tbody tr
					$('table#results tbody tr').remove();
					
					//Display rows number in the tfoot
					$('#results-number').html(datas.length);
					
					//then loop over the results
					let indice = 0;
					datas.forEach((result) => {
						results.push(result);
						
						let row = $('<tr>');
						
						//ajouter un attribut à la ligne courante
						row.attr('data-rel', indice);
						
						indice++;
						
						//1st colomn: id
						let column = $('<td>');
						column.html(result.id);
						column.appendTo(row);
						
						//2nd column : Date
						column = $('<td>');
						let option = {weekday:'long', year:'numeric', month:'long', day:'numeric'};
						column.html(new Date(result.date).toLocaleDateString('fr-FR', option));
						column.appendTo(row);
						
						//3rd column : fileNumber
						column = $('<td>');
						column.html(result.fileNumber);
						column.appendTo(row);
						
						//Add the whole row to existing DOM element
						
						$('table#results tbody').append(row);
						
					});
					
					$('#data-loader').addClass('silent');
				},
				error: (xhr, error) => {
					console.log('API call Failed', error);
					$('#data-loader').addClass('silent');
				}
			});
		}
	);
	//Detection d'un click sur une ligne du tableau
	$('table#results tbody').on(
		'click',
		'tr', //délégation d'évènement de click depuis le body sur le tr
		(event)=> {
			console.log('My logic here on :', $(event.target).parent('tr').attr('data-rel'));
			const indice = $(event.target).parent('tr').attr('data-rel');
			const result = results[indice].files;
			console.log('Files', result);
		}
		
	);
});